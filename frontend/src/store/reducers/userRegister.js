import { REGISETR, ERROR_REGISTER } from "../actions/types";

const initialState = {
  user: {},
  isRegister: null,
  error: {}
};

export default (state = initialState, action) => {
  const { type, payload } = action;
  switch (type) {
    case REGISETR: {
      return {
        ...state,
        error: null,
        user: payload,
        isRegister: true
      };
    }
    case ERROR_REGISTER:
      return {
        isRegister: false,
        error: action.payload
      };
    default:
      return state;
  }
};
