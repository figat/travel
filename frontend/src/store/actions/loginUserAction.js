import axios from "axios";
import { SET_USER_CREDENTIALS, GET_ERRORS } from "./types";

import config from '../../config';

export const userLogIn = data => async dispatch => {
  try {
      const res = await axios.post(`${config.apiUrl}/api/user/login`, data);

    const { sessionToken } = res.data;
    localStorage.setItem("token", sessionToken);

    dispatch({
      type: SET_USER_CREDENTIALS,
      payload: res.data
    });
  } catch (err) {
    //Call err============
    console.log(err);
    dispatch({
      type: GET_ERRORS,
      payload: err.response.data
    });
  }
};
