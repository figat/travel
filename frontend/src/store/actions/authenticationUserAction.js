import axios from "axios";
import { AUTH_USER } from "./types";

import config from '../../config';

export const UserAUTH = pathname => async dispatch => {
  try {
      axios.get(`${config.apiUrl}/api/user${pathname}`).then(res => {
      if (res) {
      }
      dispatch({
        type: AUTH_USER,
        payload: res.data
      });
    });
  } catch (err) {
    console.log(err);
  }
};
