import axios from "axios";
import { FETCH_USER_POST } from "./types";

import config from '../../config';

export const fetchUserPosts = () => async dispatch => {
  try {
    const jwt = localStorage.getItem("token");
      const res = await axios.get(`${config.apiUrl}/api/posts`, {
      headers: {
        "Access-Token": jwt
      }
    });
    dispatch({
      type: FETCH_USER_POST,
      payload: res.data
    });
  } catch (err) {
    console.log(err);
  }
};
