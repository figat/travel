import React, { Component, Fragment } from "react";

import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from "react-router-dom";

import LoginUserForm from "./Components/LoginForm/UserLogin";
import RegisterUserForm from "./Components/Register/Register";
import Authentication from "./Components/Authentication/Authentication";
import UserDashboardServer from "./Components/UserDashboard/UserDashboardServer";
import PrivateRoute from "./utils/PrivateRoute";
import UserPostID from "./Components/UserPostDetales/UserPostID";
import Layout from "./Containers/Layout/Layout";
import IndexNews from "./Containers/IndexNews/index";
import Japan from "./Containers/Japan/japan";
import Poland from "./Containers/Poland/poland";
import Taiwan from "./Containers/Taiwan/taiwan";
import { connect } from "react-redux";
import { userLogIn } from "./store/actions/loginUserAction";

import "./App.css";

class App extends Component {
  render() {
    const token = localStorage.getItem("token");
    return (
      <div className="app">
        <Router>
          <Fragment>
            <div className="page">
              <Switch>
                <Route exact path="/" component={Layout} />
                <Route
                  exact
                  path="/login"
                  render={() =>
                    token ? <Redirect to="/dashboard" /> : <LoginUserForm />
                  }
                />
                <Route
                  exact
                  path="/register"
                  render={() =>
                    token ? <Redirect to="/dashboard" /> : <RegisterUserForm />
                  }
                />

                <Route path="/news" component={IndexNews} />
                <Route path="/poland" component={Poland} />
                <Route path="/taiwan" component={Taiwan} />
                <Route path="/japan" component={Japan} />
                <Route
                  path="/activate/:token"
                  exact
                  component={Authentication}
                />
                <PrivateRoute
                  exact
                  path="/dashboard/:id"
                  component={UserPostID}
                />
                <PrivateRoute
                  exact
                  path="/dashboard"
                  component={UserDashboardServer}
                />
              </Switch>
            </div>
          </Fragment>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.userCREDENTIALS.user,
  isLogin: state.userCREDENTIALS.isLogin
});

export default connect(mapStateToProps, { userLogIn })(App);
