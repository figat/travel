import React, { Fragment } from "react";
import Moment from "react-moment";
import { Link } from "react-router-dom";

import "./card.css";
export const Card = ({ posts }) => {
  return (
    <Fragment>
      <div className="card">
        {posts.map((item, _id) => (
          <Link to={`/dashboard/${item._id}`}>
            <div key={_id} className="card-container">
              <div className="created">
                <Moment fromNow>{item.createdAt}</Moment>
              </div>
              <p>{item.city}</p>
            </div>
          </Link>
        ))}
      </div>
    </Fragment>
  );
};
