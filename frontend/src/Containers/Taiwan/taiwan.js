import React, { Component } from "react";
import { Link } from "react-router-dom";
import { FaRegHeart, FaRegCommentAlt } from "react-icons/fa";
import { FiLogIn } from "react-icons/fi";
import DialogBoxHelper from "../DialogBoxHelper/dialogBoxHelper";
import { taiwanContent, imgTaiwan } from "../DialogBoxHelper/index";

import "./taiwan.css";

class Taiwan extends Component {
  state = {
    open: false
  };

  handleClickOpen = e => {
    return this.setState({ open: true });
  };

  handleClicClose = () => {
    return this.setState({ open: false });
  };

  render() {
    const { open } = this.state;
    return (
      <div className="news-detail-container">
        <DialogBoxHelper
          images={imgTaiwan}
          content={taiwanContent}
          open={open}
          handleClicClose={this.handleClicClose}
        />
        <div className="news-detail-content">
          <div className="news-content-img-responsive img-responsive-first"></div>
          <div className="news-detail-info">
            <section className="news-detail-first">
              <h3 className="news-detail-firts-title">Taiwan</h3>
            </section>
            <section
              className="news-detail-second"
              onClick={this.handleClickOpen}
            >
              <p className="news-detail-second-title">
                Taiwan, officially the Republic of China, is a state in East
                Asia. Neighbouring states include the People's Republic of China
                to the north-west, Japan to the north-east, and the Philippines
                to the south....
              </p>
            </section>
            <section className="news-detail-third">
              <FaRegHeart />
              <FaRegCommentAlt />

              <Link to="/">
                {" "}
                <FiLogIn />
              </Link>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default Taiwan;
