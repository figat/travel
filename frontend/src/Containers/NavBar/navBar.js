import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { userLogIn } from "../../store/actions/loginUserAction";
import { logout } from "../../store/actions/userLogoutAction";
import "./navBar.css";

const Landing = ({ logout }) => {
  const token = localStorage.getItem("token");
  return (
    <header className="main-header">
      <nav className="main_nav">
        <Link to="/" className="brand-logo">
          <h2 className="logo-title">GUIDE</h2>
        </Link>

        <ul className="list">
          {!token ? (
            <Fragment>
              <Link to="/login">Log in</Link>
              <Link to="/register">Sign Up</Link>
            </Fragment>
          ) : (
            <Fragment>
              <Link to="/dashboard">Dashboard</Link>
              <button className="cBtn" onClick={logout}>
                Log out
              </button>
            </Fragment>
          )}
        </ul>
      </nav>
    </header>
  );
};

const mapStateToProps = state => ({
  userCREDENTIALS: state.userCREDENTIALS
});

export default connect(mapStateToProps, { userLogIn, logout })(Landing);
