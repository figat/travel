import React from "react";
import "./newsLetter.css";

const NewsLetter = () => {
  return (
    <div className="news-letter-container">
      <div className="news-letter-form-container">
        <h2 className="news-letter-heading">Sign up for our newsletter</h2>
        <p className="news-letter-form-title">
          Get more travel inspiration, tips and exclusive offers sent straight
          to your inbox
        </p>
        <form className="news-lettter-form">
          <input
            type="text"
            placeholder="Enter email"
            className="news-letter-email"
          />
          <button className="news-letter-btn">SING UP</button>
        </form>
      </div>
    </div>
  );
};

export default NewsLetter;
