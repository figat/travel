import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import Banner from "../../img/old-city.jpg";

import { FaFacebookSquare, FaTwitterSquare, FaInstagram } from "react-icons/fa";
import "./Banner.css";

const Layout = () => {
  return (
    <Fragment>
      <div className="main-banner">
        <div className="banner-content">
          <section className="banner-content-text">
            <div className="container-hero-title">
              <h1 className="hero-title">TRAVEL GUIDE</h1>
            </div>
            <div className="container-hero-sub-title">
              {" "}
              <p className="hero-sub-title">
                Our travel <span className="hero-span-title">guides</span> aim
                to give you the <span className="hero-span-title">best</span>{" "}
                and most up to date{" "}
                <span className="hero-span-title">information</span> on the
                major travel destinations around the world.
              </p>
            </div>
          </section>
          <picture className="banner-content-img">
            <img
              src={Banner}
              alt={"Banner"}
              className="banner-img-responsive"
            />
          </picture>
          <section className="banner-content-left">
            <Link className="banner-content-left-link" to="/">
              <FaFacebookSquare />
            </Link>
            <Link className="banner-content-left-link" to="/">
              <FaTwitterSquare />
            </Link>
            <Link className="banner-content-left-link" to="/">
              <FaInstagram />
            </Link>
          </section>
        </div>
      </div>
    </Fragment>
  );
};

export default Layout;
