import React from "react";
import "../News/news.css";
const NewsTitle = () => {
  return (
    <div className="news-title">
      <h3 className="news-content">Explore our cities</h3>
    </div>
  );
};

export default NewsTitle;
