import React, { Component } from "react";
import { FaRegHeart, FaRegCommentAlt } from "react-icons/fa";
import { FiLogIn } from "react-icons/fi";
import { Link } from "react-router-dom";
import DialogBoxHelper from "../DialogBoxHelper/dialogBoxHelper";
import { polandContent, imgTaiwan } from "../DialogBoxHelper/index";
import "../News/news.css";

// polandContent

class Poland extends Component {
  state = {
    open: false
  };

  handleClickOpen = e => {
    return this.setState({ open: true });
  };

  handleClicClose = () => {
    return this.setState({ open: false });
  };

  render() {
    const { open } = this.state;
    return (
      <div className="news-detail-container">
        <DialogBoxHelper
          images={imgTaiwan}
          content={polandContent}
          open={open}
          handleClicClose={this.handleClicClose}
        />
        <div className="news-detail-content">
          <div className="news-content-img-responsive img-responsive-first"></div>
          <div className="news-detail-info">
            <section className="news-detail-first">
              <h3 className="news-detail-firts-title">Poland</h3>
            </section>
            <section
              className="news-detail-second"
              onClick={this.handleClickOpen}
            >
              <p className="news-detail-second-title">
                Poland, officially the Republic of Poland, is a country located
                in Central Europe. It is divided into 16 administrative
                subdivisions, covering an area of 312,696 square kilometres, and
                has a largely temperate seasonal climate. With a population of
                nearly 38.5 million people, Poland is the sixth most populous
                member state of the European Union....
              </p>
            </section>
            <section className="news-detail-third">
              <FaRegHeart />
              <FaRegCommentAlt />

              <Link to="/">
                {" "}
                <FiLogIn />
              </Link>
            </section>
          </div>
        </div>
      </div>
    );
  }
}

export default Poland;
