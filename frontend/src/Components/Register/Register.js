import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { userRegister } from "../../store/actions/userREGISETRAction";
import { errMsg } from "../../helpers/helperFunc";
import { serverErr } from "../../helpers/helperFunc";

import "./Register.css";

const defaultState = {
  username: "",
  email: "",
  password: ""
};
class RegisterUserForm extends Component {
  state = { defaultState };

  handleSubmit = async e => {
    e.preventDefault();

    const data = {
      username: this.state.username,
      email: this.state.email,
      password: this.state.password
    };
    //setState(...): takes an object of state variables to update or a function which returns an object of state variables
    await this.props.userRegister(data);

    this.setState({ ...defaultState });
  };

  handlChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  render() {
    const { error, isRegister } = this.props;

    return (
      <div className="register-section">
        <div className="form">
          <div className="formBox">
            <form className="lFormBody" onSubmit={this.handleSubmit}>
              <h1 className="textFormBody">Create an account</h1>
              {error ? errMsg(error) : null}
              {error ? serverErr(error) : null}
              {isRegister === true ? (
                <p className="confirm">Please confirm registration by email</p>
              ) : null}
              <div className="form-container">
                <div className="input-container">
                  <input
                    className="credential-input form-control--first"
                    placeholder="First Name"
                    id="username"
                    name="username"
                    type="text"
                    value={this.state.username || ""}
                    onChange={this.handlChange}
                  />
                </div>
                <div className="input-container form-control-middle">
                  <input
                    className="credential-input"
                    placeholder="Email address"
                    id="email"
                    name="email"
                    type="email"
                    value={this.state.email || ""}
                    onChange={this.handlChange}
                  />
                </div>
                <div className="input-container">
                  <input
                    className="credential-input form-control-last"
                    placeholder="Password"
                    id="password"
                    name="password"
                    type="password"
                    value={this.state.password || ""}
                    onChange={this.handlChange}
                  />
                </div>

                <div className="input-container">
                  <input className="btn-submit" type="submit" value="Submit" />
                </div>
                <div className="form-footer">
                  <p>
                    Already have an account?{" "}
                    <Link className="form-footer-link" to="/login">
                      Log in
                    </Link>{" "}
                  </p>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div className="register-form-img-container">
          <div className="img-small-box"></div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ userRegister: { user, error, isRegister } }) => ({
  user,
  error,
  isRegister
});

export default connect(mapStateToProps, { userRegister })(RegisterUserForm);
