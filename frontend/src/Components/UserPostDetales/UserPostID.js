import React, { Component, Fragment } from "react";
import ReactMapboxGl, {
  Popup,
  ZoomControl,
  ScaleControl,
  Marker
} from "react-mapbox-gl";
import axios from "axios";
import NavBar from "../../Containers/NavBar/navBar";
import "./UserPostID.css";

import config from '../../config';

const Map = ReactMapboxGl({
  accessToken:
    "pk.eyJ1IjoiZmlnYXQiLCJhIjoiY2szNTZoeWtlMWN1bDNicGF6NDkzZ2R6ayJ9.vowgz3d6eY5OJ1aBwIeN8w"
});

class UserPostID extends Component {
  state = {
    details: []
  };

  async componentDidMount() {
    try {
      const { match } = this.props;
      const { id } = match.params;
      const jwt = localStorage.getItem("token");
        const post = await axios.get(`${config.apiUrl}/api/details/${id}`, {
        headers: {
          "Access-Token": jwt
        }
      });
      const { details } = post.data;
      this.setState({ details });
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    const { details } = this.state;
    const { events } = details;
    const { foursquare } = details;

    console.log("details :", details);
    console.log("events :", events);
    console.log("foursquare :", foursquare);

    let arr = [];
    for (let key in events) {
      arr.push(events[key]);
    }

    const mapStyle = `mapbox://styles/mapbox/streets-v9`;
    const styleCont = {
      height: "60vh",
      width: "100%",
      margin: "0",
      padding: "0",
      borderRadius: "20px"
    };
    return (
      <Fragment>
        <NavBar />
        <div className="map">
          <Map
            style={mapStyle}
            containerStyle={styleCont}
            center={details.location}
            zoom={[5]}
            attributionContro={true}
            dragRotate={true}
          >
            <ZoomControl />
            <ScaleControl measurement="mi" />
            <Marker coordinates={details.location} anchor="bottom" />
            <Popup
              coordinates={details.location}
              offset={{
                "bottom-left": [12, -38],
                bottom: [0, -38],
                "bottom-right": [-12, -38]
              }}
            >
              <h1>{details.city}</h1>
            </Popup>
          </Map>
        </div>
      </Fragment>
    );
  }
}

export default UserPostID;
