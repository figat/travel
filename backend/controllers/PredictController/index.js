require("dotenv").config({ path: "../../.env" });
const fetch = require("node-fetch");

const phq = require("predicthq");

let client = new phq.Client({
  access_token: process.env.ACCESS_TOKEN,
  fetch: fetch
});

module.exports = async (
  city,
  query = ["conferences", "festivals", "performing-arts", "concerts"]
) => {
  try {
    const response = await client.events.search({ q: city, category: query });
    const { results } = response.result;

    const res = results.map(items => {
      return {
        
        location: items.location,
        category: items.category,
        title: items.title,
        description: items.description,
        location: items.location
      };
    });

    return res;
  } catch (err) {
    console.log(err);
  }
};
