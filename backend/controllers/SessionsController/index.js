const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../../models/User");

const secret = "secret";

const create = async credentials => {
  const { email: userEmail, password } = credentials;
  try {
    const user = await User.findOne({ userEmail });

    const match = await bcrypt.compare(password, user.userPassword);
    if (match)
      return {
        sessionToken: jwt.sign(
          {
            data: user._id
          },
          secret
        ),
        id: user._id
      };
    return null;
  } catch (error) {
    throw Error(error);
  }
};

const destroy = () => ({
  sessionToken: null
});

module.exports = {
  create,
  destroy
};
