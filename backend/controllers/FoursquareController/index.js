const fetch = require("node-fetch");
require("dotenv").config({ path: "../../.env" });

module.exports = async (
  city,
  category = [
    "Hotel",
    "Bar",
    "Coffee",
    "Airports",
    "Museum",
    "College & University",
    "Event"
  ]
) => {
  try {
    const response = await fetch(
      `https://api.foursquare.com/v2/venues/explore?client_id=${process.env.CLIENT_ID}&client_secret=${process.env.CLIENT_SECRET}&limit=10&query=${category}&near=${city}&v=20180323`
    );

    const json = await response.json();
    const { groups } = json.response;

    const result = groups[0].items.map(first => {
      return {
        name: first.venue.name,
        city: first.venue.location.city,
        categories: first.venue.categories[0].name,
        address: first.venue.location.address,
        location: [first.venue.location.lat, first.venue.location.lng]
      };
    });

    return result;
  } catch (error) {
    console.log(error);
  }
};
