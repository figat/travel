require("dotenv").config();
const geo = require("@mapbox/mapbox-sdk/services/geocoding");
const style = require("@mapbox/mapbox-sdk/services/styles");
const tiles = require("@mapbox/mapbox-sdk/services/tilequery");

const tilequeryClient = tiles({ accessToken: process.env.MAPBOX_API_KEY });

tilequeryClient
  .listFeatures({
    mapIds: ["mapbox.mapbox-streets-v8"],
    coordinates: [5.0913, 51.55551],
    radius: 1000
  })
  .send()
  .then(response => {
    const features = response.body;
    console.log(features);
  });

// const mbxGeocoding = geo({accessToken: process.env.MAPBOX_API_KEY});
// mbxGeocoding.forwardGeocode({
//     query: 'Paris France',
//     limit: 2
// })
// .send()
// .then(res => console.log(res.body.features[1].context))

// const clientStyle = style({accessToken: process.env.MAPBOX_API_KEY});
// clientStyle.getStyle({
//     styleId: "cige81msw000acnm7tvsnxcp5"
// })
// .send()
// .then(res => console.log(res))
// .catch(err => console.log(err));

console.log(process.env.MAPBOX_API_KEY);
