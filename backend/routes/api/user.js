const express = require("express");
const router = express.Router();
const prepareUser = require("../../middleware/prepareUser");
const RegistrationsController = require("../../controllers/RegistrationsController");
const SessionsController = require("../../controllers/SessionsController");
const UsersController = require("../../controllers/UsersController");
const bcrypt = require("bcrypt");

const { protectedRoutes } = require("../../middleware/ProtectedRoutes");
const User = require("../../models/User");

const { check, validationResult } = require("express-validator");
const validRegister = [
  check(`username`, "Name is required")
    .not()
    .isEmpty(),
  check(`email`, "Please fill out a valid email address").isEmail(),
  check(
    `password`,
    "Please enter a password with 6 or more characters"
  ).isLength({ min: 6 })
];

const validLogin = [
  check(`email`, "Plase include valid email").isEmail(),
  check(`password`, "Password*")
    .not()
    .isEmpty()
];
router.get("/", protectedRoutes, async (req, res) => {
  res.json(await UsersController.index());
});

//User findOne by id
router.get("/dashboard", protectedRoutes, async (req, res) => {
  const user = await User.findOne(
    { _id: req.user.data },
    { userPassword: 0, authToken: 0, isAuthenticated: 0 }
  );
  return res.json(user);
});

router.post("/register", validRegister, prepareUser, async (req, res) => {
  const { email } = req.body;
  console.log(req.body);
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  const emailValid = await User.findOne({ userEmail: email });

  if (emailValid) {
    res.status(400).json({ error: "Something went wrong" });
  } else {
    try {
      const userResult = await UsersController.create(req.user);
      const registerResult = await RegistrationsController.create(req.user);
      res.json({
        userResult,
        registerResult
      });
    } catch (error) {
      res
        .status(500)
        .json({ error: "You have entered an invalid email or password" });
    }
  }
});

router.post("/login", validLogin, async (req, res) => {
  const credentials = req.body;
  const { email } = req.body;
  const { password } = req.body;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).json({ errors: errors.array() });
  }

  try {
    const user = await User.findOne({ userEmail: email });
    const match = user && (await bcrypt.compare(password, user.userPassword));

    if (!match) {
      return res
        .status(400)
        .json({ error: "You have entered an invalid email or password" });
    } else if (user.authToken !== null) {
      res.status(401).json({ error: "Please confirm registration by email" });
    } else {
      res.json(await SessionsController.create(credentials));
    }
  } catch (err) {
    
    res
      .status(500)
      .json({ error: "You have entered an invalid email or password" });
  }
});

router.get("/activate/:token", async (req, res) => {
  try {
    const registerResult = await RegistrationsController.update(
      req.params.token
    );
    res.json(registerResult);
  } catch (err) {
    res.status(400).json({
      error: "Something went wrong"
    });
  }
});

router.delete("/", async (req, res) => {
  const deleteResult = await UsersController.destroy(req.body.id);
  res.json(deleteResult);
});

module.exports = router;
