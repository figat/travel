const { check } = require("express-validator");

const validationRegister = [
  check(`username`, "Name is require")
    .not()
    .isEmpty(),
  check(`email`, "Plase include valid email").isEmail(),
  check(
    `password`,
    "Plase enter a password with 6 or more characters"
  ).isLength({ min: 6 })
];
