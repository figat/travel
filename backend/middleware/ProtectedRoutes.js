const jwt = require("jsonwebtoken");

module.exports.protectedRoutes = (req, res, next) => {
  // console.log(req.headers);
  // middleware function
  //check header for the token
  const token = req.headers["access-token"];
  const secret = "secret";
  // decode token
  if (!token) {
    return res.status(401).json({ error: "No token" });
  }

  try {
    // verifies secret and checks if the token
    const decode = jwt.verify(token, secret);
    req.user = decode;
    next();
  } catch (error) {
    res.status(400).json({ err: "Token not valid" });
    throw Error(error);
  }
};
