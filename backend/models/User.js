const mongoose = require("mongoose");

// Create Schema

const User = new mongoose.Schema({
  userName: { type: String },
  userEmail: { type: String, required: true, unique: true, trim: true },
  userPassword: { type: String, required: true },
  authToken: { type: String, required: true, default: null },
  isAuthenticated: { type: Boolean, required: true, default: false },
  posts: [
    {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Post"
    }
  ]
});

module.exports = mongoose.model("User", User);
